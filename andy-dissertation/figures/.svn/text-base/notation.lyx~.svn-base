#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="8" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Subgrids:
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\sigma$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A subgrid 
\begin_inset Formula $\sigma=(s,n,m)$
\end_inset

 where 
\begin_inset Formula $s$
\end_inset

 is a value identifying a subgrid, 
\begin_inset Formula $n$
\end_inset

 is the width of the subgrid and m is the height of the subgrid.
 
\series bold
(Equation 
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:subgrid}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $I\left(\sigma\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Function to calculate index set of a subgrid (indices are in the grid domain).
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:subgrid_indices}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $H(\sigma)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Function to calculate set of indices for halo of a subgrid (indices are
 in the grid domain).
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:subgrid_halo}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $B(\sigma)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Function to calculate set of indices for border of a subgrid (indices are
 in the grid domain).
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:subgrid_border}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 2em
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="8" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Neighborhoods
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\nu$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A neighbor 
\begin_inset Formula $\nu=\left\langle i,j\right\rangle $
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:neighbor}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $N$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A neighborhood, which is a list of neighbors ordered in a clockwise fashion.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:neighborhood}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $r\left(N,\nu,n\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Rotation function: given a neighbor 
\begin_inset Formula $\nu$
\end_inset

 return the neighbor that is visited by doing 
\begin_inset Formula $n$
\end_inset

 clockwise rotations in the neighborhood 
\begin_inset Formula $N$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:rotation}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $I_{lcl}\left(\left\langle i,j\right\rangle ,d\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A depth-d domain of local indices for a stencil operation centered at point
 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $\left\langle i,j\right\rangle $
\end_inset

.
\family default
\series bold
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:stencil_domain}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset VSpace 2em
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="12" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Border mappings:
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\beta$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A border map 
\begin_inset Formula $\beta=\left(\rho_{tgt},\rho_{src}\right)$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:bmap}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\beta'$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A border map cognizant of rotation (used in Chapter) 
\begin_inset Formula $\beta=\left(\rho_{tgt},\rho_{src},n\right)$
\end_inset

 where n is specifies how much neighbors to rotate through in some neighborhood
 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $N$
\end_inset


\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
 where 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $0\leq n\leq|N|$
\end_inset

.
 
\family default
\series bold
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:bmap_adv}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\rho_{tgt}$
\end_inset

,
\begin_inset Formula $\rho_{src}$
\end_inset

 
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Target and source rectangles of a mapping.
 
\series bold
(Equations
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eqn:rho_tgt_cst}
\end_layout

\end_inset

 and
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eqn:rho_src_cst}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset space \hspace{}
\length 4em
\end_inset


\begin_inset Formula $\rho_{tgt}=\left(s_{1},p_{i},p_{j},q_{i},q_{j}\right)$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset space \hspace{}
\length 4em
\end_inset


\begin_inset Formula $\rho_{src}=\left(s_{2},v_{i},v_{j},w_{i},w_{j}\right)$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $o(\rho)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Orientation of some rectangle 
\begin_inset Formula $\rho$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:orientation}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $m(\beta,s,\left\langle i,j\right\rangle )$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A border mapping function.
 The function uses 
\begin_inset Formula $\beta$
\end_inset

 to determe what 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
some point 
\begin_inset Formula $\left(s,\left\langle i,j\right\rangle \right)$
\end_inset

 in a subgrid's halo should map to.
 
\family default
\series bold
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:bmap}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 2em
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="8" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Grids
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\gamma$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A grid, 
\begin_inset Formula $\gamma=\left(S,B\right)$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:grid}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
\begin_inset Formula $S$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A set of subgrids.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
\begin_inset Formula $B$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A set of border maps
\series bold
.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $I\left(\gamma\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A function that calculates the indices in grid 
\begin_inset Formula $\gamma$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:grid_indices}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 2em
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="16" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Decompositions
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $D$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A decomposition, which is a set of blocks.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\theta$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A block 
\begin_inset Formula $\theta=\left(g,l,p,s,\left\langle u_{i},u_{j}\right\rangle ,\left\langle v_{i},v_{j}\right\rangle \right)$
\end_inset

 where 
\begin_inset Formula $g$
\end_inset

 is a global block identifier, 
\begin_inset Formula $l$
\end_inset

 is a local block identifier, 
\begin_inset Formula $p$
\end_inset

 is a process identifier (MPI rank), 
\begin_inset Formula $s$
\end_inset

, is a subgrid identifier, and 
\begin_inset Formula $\left\langle u_{i},u_{j}\right\rangle $
\end_inset

and
\begin_inset Formula $\left\langle u_{i},u_{j}\right\rangle $
\end_inset

are points in subgrid 
\begin_inset Formula $s$
\end_inset

.

\series bold
 (Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:block}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\theta'$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Updated version of a block (from Section
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{sec:ghost_cells}
\end_layout

\end_inset

 on).
 
\begin_inset Formula $\theta=\left(g,l,p,s,\left\langle u_{i},u_{j}\right\rangle ,\left\langle v_{i},v_{j}\right\rangle ,G\right)$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:block_adv}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $G$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A list of ghost-nodes for the block.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $I\left(\theta\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A function that calculates the global indices of a block 
\begin_inset Formula $\theta$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:block_indices}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $I_{lcl}\left(\theta\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A function that calculates the local indices of a block 
\begin_inset Formula $\theta$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:block_indices_lcl}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $H_{lcl}\left(\theta,d\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A function that calculates the local indices of a halo of depth d around
 block 
\begin_inset Formula $\theta$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:blk_halo}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $t(s,\left\langle i,j\right\rangle ,G)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Translate a grid index 
\begin_inset Formula $(s,\left\langle i,j\right\rangle )$
\end_inset

 into its position in 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
the list G.
 
\family default
\series bold
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:translation}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 2em
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Communication plans for halos
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\pi$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A communication plan to populate halos, 
\begin_inset Formula $\pi=\left(M_{r},M_{s}\right)$
\end_inset

where 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $M_{r}$
\end_inset

 is a set of receiving messages and 
\begin_inset Formula $M_{s}$
\end_inset

 is a set of sending messages.
 
\family default
\series bold
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:comm_plan}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mu$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A single message to populate halos 
\begin_inset Formula $\mu=\left(l,\rho,p\right)$
\end_inset

 where l is a local block identifier (that contains data to send if 
\begin_inset Formula $\mu\in M_{s}$
\end_inset

 or data to receieve if 
\begin_inset Formula $\mu\in M_{R}$
\end_inset

, p is a process ID, and 
\begin_inset Formula $\rho$
\end_inset

 is a rectangle contained either contained in 
\begin_inset Formula $i\left(\theta\right)$
\end_inset

 if 
\begin_inset Formula $\mu\in M_{s}$
\end_inset

 or 
\begin_inset Formula $h\left(\theta,d\right)$
\end_inset

 if 
\begin_inset Formula $\mu\in M_{R}$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:msg}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 2em
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1.25in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Communication plans for ghost lists
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\pi'(p)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A communication plan to populate ghost lists, 
\begin_inset Formula $\pi(p)=\left(M'_{r},'M_{s}\right)$
\end_inset

where 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $M'_{r}$
\end_inset

 is a set of receiving messages for processor 
\begin_inset Formula $p$
\end_inset

 and 
\begin_inset Formula $M'_{s}$
\end_inset

 is a set of sending messages for processor 
\begin_inset Formula $p$
\end_inset

.
 
\family default
\series bold
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:comm_plan_ghosts}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mu'$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A single message to populate halos 
\begin_inset Formula $\mu=\left(p,G\right)$
\end_inset

 where 
\begin_inset Formula $p$
\end_inset

 is the processor the message is being received from or sent to, and G is
 the list of ghost nodes that are transfered.

\series bold
 (Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:comm_plan_ghosts}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $m(\gamma,\theta,\left\langle i,j\right\rangle ,\left\langle i',j'\right\rangle )$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Maps a local index 
\begin_inset Formula $\left\langle i,j\right\rangle $
\end_inset

 in the halo 
\begin_inset Formula $h_{lcl}\left(\theta,d\right)$
\end_inset

 for a block 
\begin_inset Formula $\theta$
\end_inset

 to a global index in grid 
\begin_inset Formula $\gamma$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:halo_map}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 2em
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
noindent
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="2" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="top" width="1in">
<column alignment="left" valignment="top" width="5in">
<row>
<cell multicolumn="1" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Data Objects
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row topspace="0.5em">
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\delta$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A data object 
\begin_inset Formula $\delta=\left(D,d\right)$
\end_inset

.
 
\series bold
(Equation
\begin_inset ERT
status open

\begin_layout Plain Layout

~
\backslash
ref{eq:data}
\end_layout

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_body
\end_document
