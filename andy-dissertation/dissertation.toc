\contentsline {chapter}{ABSTRACT}{ii}{section*.2}
\contentsline {chapter}{ACKNOWLEDGEMENTS}{iii}{section*.3}
\contentsline {chapter}{TABLE OF CONTENTS}{iv}{section*.4}
\contentsline {chapter}{LIST OF FIGURES}{viii}{section*.5}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Research problem}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Approach}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Research contributions}{7}{section.1.3}
\contentsline {section}{\numberline {1.4}Dissertation organization}{8}{section.1.4}
\contentsline {chapter}{\numberline {2}Earth-science grids and proxy applications}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Earth science grids}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Proxy applications}{13}{section.2.2}
\contentsline {section}{\numberline {2.3}The CGPOP miniapp}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Extracting POP's main kernel}{16}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Required behavior of CGPOP}{17}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Establishing CGPOP as a performance proxy}{20}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Establishing CGPOP as a programmability proxy}{23}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}The Shallow Water Model code}{24}{section.2.4}
\contentsline {section}{\numberline {2.5}Summary}{27}{section.2.5}
\contentsline {chapter}{\numberline {3}Background on programming models}{29}{chapter.3}
\contentsline {section}{\numberline {3.1}Programming models}{29}{section.3.1}
\contentsline {section}{\numberline {3.2}Implementation mechanisms}{30}{section.3.2}
\contentsline {section}{\numberline {3.3}Tangling and approaches to untangling}{32}{section.3.3}
\contentsline {chapter}{\numberline {4}Programming models for stencil computations}{37}{chapter.4}
\contentsline {section}{\numberline {4.1}Motivation for stencil computation tools}{37}{section.4.1}
\contentsline {section}{\numberline {4.2}Grid connectivity and data structure tradeoffs}{39}{section.4.2}
\contentsline {section}{\numberline {4.3}Comparing existing stencil tools}{43}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Mechanism}{43}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Grid type and data structure}{44}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Stencil type}{45}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Stencil specification}{46}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Connectivity specification}{46}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}Iteration specification}{47}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}Decomposition specification}{47}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}Targets}{48}{subsection.4.3.8}
\contentsline {section}{\numberline {4.4}Summary}{49}{section.4.4}
\contentsline {chapter}{\numberline {5}Semiregular-grid abstractions}{51}{chapter.5}
\contentsline {section}{\numberline {5.1}User view of abstractions}{51}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Grid connectivity}{51}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Grid decomposition}{55}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Stencil algorithms}{56}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Formalizing and implementing abstractions}{58}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Handling non-standard stencil nodes}{58}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Communication planning for halos}{58}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Formalisms for semiregular grid abstractions}{59}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Algorithm and formalisms for communication}{62}{subsection.5.2.4}
\contentsline {chapter}{\numberline {6}Addressing non-compact stencils}{65}{chapter.6}
\contentsline {section}{\numberline {6.1}Halos and index spaces}{67}{section.6.1}
\contentsline {section}{\numberline {6.2}Naive halo expansion}{69}{section.6.2}
\contentsline {section}{\numberline {6.3}Expansion with rotation}{73}{section.6.3}
\contentsline {section}{\numberline {6.4}Ghost cells}{76}{section.6.4}
\contentsline {section}{\numberline {6.5}Data access in non-compact stencils}{77}{section.6.5}
\contentsline {section}{\numberline {6.6}Performance impacts}{77}{section.6.6}
\contentsline {chapter}{\numberline {7}Performance optimizations}{80}{chapter.7}
\contentsline {section}{\numberline {7.1}Inlining stencil code}{80}{section.7.1}
\contentsline {section}{\numberline {7.2}Communication avoiding}{82}{section.7.2}
\contentsline {chapter}{\numberline {8}CGPOP and SWM case studies}{85}{chapter.8}
\contentsline {section}{\numberline {8.1}Using GridWeaver in CGPOP}{85}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Decomposition and data}{87}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Implementing CGPOP operations}{92}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Transition from dipole to tripole grid}{92}{subsection.8.1.3}
\contentsline {subsection}{\numberline {8.1.4}Impact on performance and programmability}{94}{subsection.8.1.4}
\contentsline {section}{\numberline {8.2}Using GridWeaver in SWM's horizontal-advection operation}{96}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Modeling the icosahedral grid}{96}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Importing decomposition and data}{97}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Modeling horizontal advection operation}{98}{subsection.8.2.3}
\contentsline {subsection}{\numberline {8.2.4}Impact on performance and programmability}{101}{subsection.8.2.4}
\contentsline {chapter}{\numberline {9}The GridWeaver active library}{103}{chapter.9}
\contentsline {section}{\numberline {9.1}Software architecture and package organization}{103}{section.9.1}
\contentsline {section}{\numberline {9.2}Installation}{104}{section.9.2}
\contentsline {section}{\numberline {9.3}Invocation and Use}{105}{section.9.3}
\contentsline {section}{\numberline {9.4}Conway's Game of Life on an icosahedral grid}{106}{section.9.4}
\contentsline {subsection}{\numberline {9.4.1}Rules for the Game of Life}{106}{subsection.9.4.1}
\contentsline {subsection}{\numberline {9.4.2}Modeling an icosahedral grid}{107}{subsection.9.4.2}
\contentsline {subsection}{\numberline {9.4.3}Game of life stencil function}{108}{subsection.9.4.3}
\contentsline {subsection}{\numberline {9.4.4}Compiling and executing}{111}{subsection.9.4.4}
\contentsline {subsection}{\numberline {9.4.5}Summary}{111}{subsection.9.4.5}
\contentsline {chapter}{\numberline {10}Current limitations and future work}{113}{chapter.10}
\contentsline {section}{\numberline {10.1}Relaxing implementation constraints}{113}{section.10.1}
\contentsline {section}{\numberline {10.2}Additional optimizations and target architectures}{114}{section.10.2}
\contentsline {section}{\numberline {10.3}Identifying regularity in existing meshes}{116}{section.10.3}
\contentsline {section}{\numberline {10.4}Visualizing grid connectivity}{117}{section.10.4}
\contentsline {chapter}{\numberline {11}Conclusions}{118}{chapter.11}
\contentsline {chapter}{References}{121}{chapter*.68}
\contentsline {chapter}{Appendix \numberline {A}Notation}{134}{Appendix.a.A}
\contentsline {chapter}{Appendix \numberline {B}Application programming interface}{138}{Appendix.a.B}
\contentsline {chapter}{Appendix \numberline {C}Supplementary figures}{148}{Appendix.a.C}
