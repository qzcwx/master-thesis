
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Thesis and Dissertation Document Class %%
%% Colorado State University              %%
%% Spring 2011                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Original LaTeX2.09 style created by
%%     Thad Mauney     Fall l984                              
%% Revised 
%%     Scott Douglas   Summer 1985
%%     Gary Herron     June 1985
%%     P. Fitzhorn     June 1985
%%     Garry Herron    December 1985
%%     Tom Wood        December 1985
%% Original LaTeX2e document class created by
%%     Larry Pyeatt    August 1997
%% Adapted for new ETD submission standards by
%%     Andrew Sutton   April 2011
%%     Fadi Wedyan     May 2011
%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{dissertation}[2011/04/01 Colorado State University Dissertation class]
\DeclareOption{twocolumn}{\OptionNotUsed}
\DeclareOption{titlepage}{\OptionNotUsed}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}

%%% Spacing commands
\newcommand\@textlinespacing{}
\newcommand{\singlespace}{\@normalsize \baselineskip \normalbaselineskip}
\newcommand{\doublespace}{\@normalsize \baselineskip 1.6\normalbaselineskip}
\renewcommand{\@textlinespacing}{\doublespace}
\newcommand{\headingskip}{\vskip 10pt \par}
\DeclareOption{savepaper}
{
  \renewcommand\@textlinespacing{\singlespace}
}

\ProcessOptions\relax

\RequirePackage{ifthen}
\RequirePackage{remreset}

\LoadClass{report}

%%% Footnotes and endnotes must be numbered consecutively throughout
%%% the document
\@removefromreset{footnote}{chapter}
\@removefromreset{endnote}{chapter}

\newcommand{\undefo}[1]{
  \framebox{\texttt{\textbackslash\lowercase\expandafter{#1}} undefined}}

\newcommand{\doctype}[1]{\gdef\Zdoctype{#1}}
\doctype{\undefo{doctype}}

\renewcommand{\title}[1]{\gdef\Ztitle{#1}}
\title{\undefo{title}}

\renewcommand{\author}[1]{\gdef\Zauthor{#1}}
\author{\undefo{author}}

\newcommand{\degree}[1]{\gdef\Zdegree{#1}}
\degree{\undefo{degree}}

\newcommand{\semester}[1]{\gdef\Zsemester{#1}}
\semester{\undefo{semester}}

%\renewcommand{\year}[1]{\xdef\Zyear{#1}}
%\year{\undefo{year}}

\newcommand{\department}[1]{\gdef\Zdepartment{#1}}
\department{\undefo{department}}

\newcommand{\signaturedate}[1]{\gdef\Zsignaturedate{#1}}
\signaturedate{\undefo{signaturedate}}

%%% COMMITTEE
\newcommand{\committee}[1]{\gdef\Zcommittee{#1}}
\committee{\item \undefo{committee}}

\newenvironment{committeelist}{
  \begin{list}{}{
      \setlength{\leftmargin}{.5in}
      \setlength{\topsep}{0 in}
      \setlength{\parskip}{0 in}
      \setlength{\itemsep}{0 in}
      \setlength{\parsep}{0 in}
      \setlength{\labelwidth}{.77 in}
      \setlength{\rightmargin}{.15 in}
    }
  }{\end{list}
}

%%% PRELIMINARIES
\newcommand{\preliminaries}{\doublespace\pagestyle{plain}
  \pagenumbering{roman}\setcounter{page}{1}}

%%% BODY
\newcommand{\body}{
  %\doublespace\eject %%% XXX savepaper
  \@textlinespacing\eject
  \pagenumbering{arabic}
  \setcounter{page}{1}\eject
}

%%% SUPPLEMENTARIES
\newcommand{\supplementaries}{\renewcommand{\@chapapp}{Appendix}
  \setcounter{chapter}{0}\renewcommand{\thechapter}{\Alph{chapter}}}

%%% MAKETITLE
\renewcommand{\maketitle}{%
\thispagestyle{empty}
        \begin{center}
              \uppercase\expandafter{\Zdoctype}\\[\baselineskip]
%\singlespace
\doublespace
              {\uppercase\expandafter{\Ztitle}}\\[5\baselineskip]
              Submitted by\\
              \Zauthor\\
              \Zdepartment\\[4\baselineskip]
              In partial fulfillment of the requirements\\
              for the Degree of \Zdegree\\
              Colorado State University\\
              Fort Collins, Colorado\\
              \Zsemester\ \Zyear\\
%              \null
       \end{center} 
\vskip 12pt
\singlespace Doctoral Committee:\\ \begin{committeelist} \Zcommittee \end{committeelist}
\newpage\doublespace}

%%% COPYRIGHT
\newcommand{\makecopyright}{\setcounter{page}{1}\thispagestyle{empty}
\null \vfill\parbox{\textwidth}{\begin{center}
\doublespace 
Copyright \copyright\ \Zauthor\ \Zyear\\ All Rights Reserved\end{center}}
\vfill\newpage}

%%% SIGNATURE PAGE (no longer required Spring 2011)
\newcommand{\makesignature}[2][1]{
\doublespace\setcounter{page}{2}
\newcounter{members}
\setcounter{members}{-1}
\newcounter{coadvisers}
\setcounter{coadvisers}{1}
    \newpage\null\vskip 1in
    \begin{center} COLORADO STATE UNIVERSITY \\[\baselineskip] \end{center}
    \begin{flushright}
\Zsignaturedate
\end{flushright}
    \par  WE HEREBY RECOMMEND THAT THE \uppercase\expandafter {\Zdoctype}\
    PREPARED UNDER OUR SUPERVISION BY
    \uppercase\expandafter {\Zauthor}\
    ENTITLED \uppercase\expandafter {\Ztitle}\
    BE ACCEPTED AS FULFILLING IN PART REQUIREMENTS FOR THE
    DEGREE OF \uppercase\expandafter {\Zdegree}.\sloppy\par
    \begin{center}
    \vskip 1\baselineskip
    \underline{Committee on Graduate Work} \\[1\baselineskip]
\whiledo{\value{members}<#2}
{\rule{3.5in}{1pt}\\[-\normalbaselineskip]
 \makebox[3.5in][l]{Committee Member}
\stepcounter{members}}
\ifthenelse{#1>1}{
\whiledo{\value{coadvisers}<#1}
{\rule{3.5in}{1pt}\\[-\normalbaselineskip]
 \makebox[3.5in][l]{TBD-Adviser}
 \stepcounter{coadvisers}}
}
{\rule{3.5in}{1pt}\\[-\normalbaselineskip]
 \makebox[3.5in][l]{Adviser}}
{\rule{3.5in}{1pt}\\[-\normalbaselineskip]
 \makebox[3.5in][l]{Co-Adviser}}
    \rule{3.5in}{1pt}\\[-\normalbaselineskip]
    \makebox[3.5in][l]{Department Head}
    \end{center}
    \newpage
}

%%% ABSTRACT
\renewenvironment{abstract}{
  \newpage\null\vskip 54pt
  \setcounter{page}{2} %% --ams 2011-03-25
  %% \centerline {ABSTRACT OF \uppercase\expandafter{\Zdoctype}}
  \centerline {ABSTRACT} %% --ams 2011-03-03
  \headingskip
  \begin{center}
    \uppercase\expandafter{\Ztitle}
  \end{center}
  \headingskip
  %%\doublespace %%% XXX savepaper 
  \@textlinespacing
}{% \vskip 15pt
  % \rightline{\vbox{\baselineskip 12pt
  %     \hbox{\Zauthor}
  %     \hbox{\Zdepartment}
  %     \hbox{Colorado State University}
  %     \hbox{Fort Collins, CO 80523}
  %     \hbox{\Zsemester\ \Zyear}}} %% No closing after abstract --ams 3/28/2011
  \newpage
}

%%% ACKNOWLEDGEMENTS
\newenvironment{acknowledgements}{
  \newpage\null\vskip 54pt
  \centerline {ACKNOWLEDGEMENTS} 
  \headingskip
  \@textlinespacing
}{
  \par\vfill
%  \begin{center}
%    \fbox{\footnotesize This document was prepared and typeset by the
%      author in \LaTeX}
%  \end{center}

  \newpage
}

%%% PREFACE
\newenvironment{preface}{
  \newpage\null\vskip 54pt
  \begin{center} PREFACE \end{center}
  \headingskip
}{\newpage}

%%% AUTOBIOGRAPHY
\newenvironment{autobiography}{
  \newpage\null\vskip 54pt
  \begin{center} AUTOBIOGRAPHY \end{center}
  \headingskip
}{\newpage}

%%% DEDICATION
\newenvironment{dedication}{
  \newpage\null\vskip 54pt
%  \begin{center} DEDICATION \end{center}
  \headingskip
}{\newpage}

%%% TABLE OF CONTENTS
\renewcommand{\tableofcontents}{
  \newpage
  \begin{center}TABLE OF CONTENTS\end{center}
  \headingskip
  \@textlinespacing
  \@starttoc{toc}
  \doublespace
}

%%% LOT
\renewcommand{\listoftables}{
  \newpage\null\vskip54pt
  \begin{center}LIST OF TABLES\end{center}
  \headingskip
  \@textlinespacing
  \@starttoc{lot}
  \doublespace
}

%%% LOF
\renewcommand{\listoffigures}{
  \newpage\null\vskip54pt
  \begin{center}LIST OF FIGURES\end{center}
  \vskip20pt
  \@textlinespacing
  \@starttoc{lof}
  \doublespace
}

%%% LIST OF SYMBOLS
\newenvironment{listofsymbols}{
  \newpage\null\vskip 54 pt
  \begin{center} LIST OF SYMBOLS AND ACRONYMS \end{center}
  \headingskip
}{\newpage}

%%% LIST OF KEYWORDS
\newenvironment{listofkeywords}{
  \newpage\null\vskip 54 pt
  \begin{center} LIST OF KEYWORDS \end{center}
  \headingskip
}{\newpage}

%%% BIBLIOGRAPHY
\renewcommand{\thebibliography}[1]{%
{%\let\@schapter=\@refchapter
\chapter*{References}}
\addcontentsline{toc}{chapter}{References} 
\singlespace
\list{[\arabic{enumi}]}{\settowidth\labelwidth{[#1]}
\leftmargin\labelwidth \advance\leftmargin\labelsep
\usecounter{enumi}}
\def\newblock{\hskip .11em plus .33em minus -.07em}
\sloppy
\sfcode `\.=1000\relax}

%%% APPENDIX
\renewcommand{\appendix}[1]{\chapter{#1}}

%%% GLOSSARY
\newenvironment{gloss}{
  \newpage\null\vskip 54 pt
  \begin{center} GLOSSARY OF TERMS \end{center}
  \headingskip
}{\newpage}

%%% ABBREVIATIONS
\newenvironment{abbreviations}{
  \newpage\null\vskip 54 pt
  \begin{center} LIST OF ABBREVIATIONS \end{center}
  \headingskip
}{\newpage}

%%% INDICES
\newenvironment{indices}{
  \newpage\null\vskip 54 pt
  \begin{center} INDEX \end{center}
  \headingskip
}{\newpage}

%%% MARGINS, ETC
\evensidemargin 0.50in
\oddsidemargin 0.50in
\topmargin 0in
\textwidth 5.8in
\textheight 8.5in
\headheight 0in
\headsep 0in
\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5}
\footnotesep 2\baselineskip
\setlength{\skip\footins}{2\baselineskip}
\renewcommand{\footnoterule}{\noindent\vspace*{-3pt}\rule{1.5in}{0.4pt}\vspace*{2.6pt}}
\endinput

%% 
%% End of file `dissertation.cls'.
