\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.27pt}
\defcounter {refsection}{0}\relax 
\select@language {american}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{Background}{2}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Contributions}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Preliminaries}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{NK-Landscapes}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Walsh Transform}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Walsh-LS}{10}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Theory}{10}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Complexity}{23}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{Empirical Results}{25}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Walsh-LS with Surrogate Function}{27}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Motivation}{27}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{Surrogate Fitness}{29}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Conclusion}{38}{0}{5}
